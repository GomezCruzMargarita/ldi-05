/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class HexDecMenu {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         int op;
       
       Scanner entrada = new Scanner(System.in);
      do{
       System.out.println("1.- Hexadecimal a Decimal");
       System.out.println("2.- Decimal a Hexadecimal");
       System.out.println("3.- Salir");
       op=entrada.nextInt();
       
       switch(op) 
       {
           case 1:
               String numero;
               System.out.println("Ingresa el numero hexadecimal a convertir: ");
               numero = entrada.next();
               int decimal = Integer.parseInt(numero, 16);
               System.out.println("El numero hexadecimal: "+numero+" es igual en decimal a:" +decimal);
               break;
           case 2:
               int ndecimal;
               System.out.println("Ingresa el numero decimal a convertir: ");
               ndecimal = entrada.nextInt();
               String hexa = Integer.toHexString(ndecimal);
               System.out.println("El numero decimal: "+ndecimal+" es igual en decimal a:" +hexa);
               break;
       }
      }while(op != 3);
    }
    
}
